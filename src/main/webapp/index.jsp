<!DOCTYPE html>
<%@ page import="java.util.Date" session="false" %>
<%@ page import="java.util.Objects" session="false" %>
<% response.setHeader("X-Frame-Options", "DENY"); response.setHeader("Strict-Transport-Security", "max-age=31536000"); %>
<html>
<head>
<title>webclockbackend.appspot.com / appsvr.peterjin.org</title>
</head>
<body>
<h1>webclockbackend.appspot.com / appsvr.peterjin.org</h1>
<p>You have reached webclockbackend.appspot.com and/or appsvr.peterjin.org.
This (sub)domain is used to host
<a href="https://www.peterjin.org">Peter Jin</a>'s web apps.</p>
<p>The following web apps are hosted on this (sub)domain:</p>

<!-- TODO: list web apps hosted here -->
<ul>
<li>
	<span style="text-decoration: line-through;">MTD Reroutes and Vehicles API endpoint</span> - see 
<a href="https://go.peterjin.org/cumtd-api">here</a> for more information</li>
<li><a href="/a3krul">Aliases 3000 Rapid Update List</a></li>

</ul>

<p>
The identifier "webclockbackend" was originally used because this (sub)domain
originally hosted the backend for my <a href="https://webclock.peterjin.org">web
clock</a> before moving to Cloud Functions. But since Google Cloud Platform does
not allow the project ID to be changed after it has been created, this is what 
I'll have to use for now and possibly the future. As a consequence, please do
not use any URL containing the string "webclockbackend" in any long-term-use
code. The latter URL, on the other hand, is stable in the foreseeable future.
</p>

<a href="https://webclockbackend.appspot.com/">webclockbackend.appspot.com</a> /
<a href="https://appsvr.peterjin.org/">appsvr.peterjin.org</a>
<p>Copyright 2018-2020 Peter Jin. All rights reserved. View the source code for these
apps on
<a href="https://github.com/PHJArea217/appserver">GitHub</a> and
<a href="https://gitlab.com/PHJArea217/appserver">GitLab</a>.</p>

<div>Current time: <%= new Date().toGMTString() %></div>
<div>Your IP address is <%= request.getRemoteAddr() %></div>
<div>Your browser's user agent string: <%= Objects.toString(request.getHeader("user-agent"), "(none)").replaceAll("&", "&amp;").replaceAll("<", "&lt;") %></div>
</body>
</html>
